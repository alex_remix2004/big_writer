@php /** @var App\Models\Page $item */ @endphp

<div class="row justify-content-center">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title"></div>
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" href="#maindata" data-toggle="tab" role="tab">Основные данные</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#extradata" data-toggle="tab" role="tab">Доп данные</a>
                    </li>
                </ul>
                <br>
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="maindata" role="tabpanel" aria-labelledby="home-tab">
                        <div class="form-group">
                            <label for="title">Название</label>
                            <input name="title" value="{{ old('title', $item->title) }}" type="text" class="form-control" id="title" placeholder="Название"
                                minlength="3" required>
                        </div>
                        <div class="form-group">
                            <label for="description">Описание</label>
                            <textarea name="description" class="form-control" id="description" rows="2">{{ old('description', $item->description) }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="content_raw">Содержимое</label>
                            <textarea name="content_raw" class="form-control" id="content_raw" rows="20">{{ old('content_raw', $item->content_raw) }}</textarea>
                        </div>
                    </div>
                    <div class="tab-pane fade show" id="extradata" role="tabpanel" aria-labelledby="home-tab">
                        <div class="form-group">
                            <label for="title">Книга</label>
                            <select name="book_id" id="book_id" class="form-control" placeholder="Выберите книгу" required>
                                @foreach($booksList as $bookOption)
                                    <option value="{{ $bookOption->id }}" @if($bookOption->id == $item->book_id) selected @endif>
                                        {{ $bookOption->id_title }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="slug">Идентификатор</label>
                            <input name="slug" value="{{ old('slug', $item->slug) }}" type="text" class="form-control" id="slug" placeholder="Идентификатор">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
