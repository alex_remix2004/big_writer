@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                @include('admin.pages.includes.result_messages')

                <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
                    <a class="btn btn-primary" href="{{ route('admin.pages.model.create') }}">Добавить</a>
                </nav>
                <div class="card">
                    <div class="card-body">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Название</th>
                                    <th>Книга</th>
                                    <th>Добавлено</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($paginator as $page)
                                    @php /** @var App\Models\Page $page */ @endphp
                                    @php /** @var Illuminate\Pagination\Paginator $paginator */ @endphp
                                    <tr>
                                        <td>{{ $page->id }}</td>
                                        <td>
                                            <a href="{{ route('admin.pages.model.edit', $page->id) }}">{{ $page->title }}</a>
                                        </td>
                                        <td>
                                            {{--{{ $page->pageBook->title ?? 'не указано' }}--}}
                                            {{--{{ optional($page->pageBook)->title }}--}}
                                            {{ $page->pageBookTitle }}
                                        </td>
                                        <td>{{ $page->created_at->format('d.m.Y H:i') }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @if($paginator->total() > $paginator->count())
            <br>
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            {{ $paginator->links() }}
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection

