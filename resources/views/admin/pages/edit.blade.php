@extends('layouts.app')

@section('content')
<div class="container">

    @include('admin.pages.includes.result_messages')

    @php /** @var App\Models\Page $item */ @endphp
        @if($item->exists)
            <form method="POST" action="{{ route('admin.pages.model.update', $item->id) }}">
                @method('PATCH')
        @else
            <form method="POST" action="{{ route('admin.pages.model.store') }}">
        @endif
            @csrf
            <div class="row justify-content-center">
                <div class="col-lg-8 col-md-8">
                    @include('admin.pages.includes.page_edit_main_col')
                </div>
                <div class="col-lg-4 col-md-4">
                    @include('admin.pages.includes.page_edit_add_col')
                </div>
            </div>
        </form>
        @if($item->exists)
            <br>
            <form method="POST" action="{{ route('admin.pages.model.destroy', $item->id) }}">
                @method('DELETE')
                @csrf
                <div class="row justify-content-center">
                    <div class="col-lg-8 col-md-8">
                        <div class="card">
                            <div class="card-body ml-auto">
                                <button type="submit" class="btn btn-link">Удалить</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4"></div>
                </div>
            </form>
        @endif
</div>
@endsection
