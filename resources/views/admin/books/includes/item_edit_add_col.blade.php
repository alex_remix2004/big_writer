@php /** @var App\Models\Book $item */ @endphp
<div class="row justify-content-center">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-body">
                <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
        </div>
    </div>
</div><br>
@if($item->exists)
    <div class="row justify-content-center">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-body">
                    <ul class="list-unstyled">
                        <li>ID: {{ $item->id }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row justify-content-center">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="maindata" role="tabpanel" aria-labelledby="home-tab">
                            <div class="form-group">
                                <label for="created_at">Создано</label>
                                <input value="{{ $item->created_at }}" type="text" class="form-control" disabled>
                            </div>
                            <div class="form-group">
                                <label for="updated_at">Обновлено</label>
                                <input value="{{ $item->updated_at }}" type="text" class="form-control" disabled>
                            </div>
                            <div class="form-group">
                                <label for="deleted_at">Удалено</label>
                                <input value="{{ $item->deleted_at }}" type="text" class="form-control" disabled>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
