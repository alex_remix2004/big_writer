@php /** @var App\Models\Book $item */ @endphp

<div class="row justify-content-center">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title"></div>
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" href="#maindata" data-toggle="tab" role="tab">Основные данные</a>
                    </li>
                </ul>
                <br>
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="maindata" role="tabpanel" aria-labelledby="home-tab">
                        <div class="form-group">
                            <label for="title">Название</label>
                            <input name="title" value="{{ old('title', $item->title) }}" type="text" class="form-control" id="title" placeholder="Название"
                                minlength="3" required>
                        </div>
                        <div class="form-group">
                            <label for="slug">Идентификатор</label>
                            <input name="slug" value="{{ old('slug', $item->slug) }}" type="text" class="form-control" id="slug" placeholder="Идентификатор">
                        </div>
                        <div class="form-group">
                            <label for="user_id">Автор</label>
                            <select name="user_id" id="user_id" class="form-control" placeholder="Выберите пользователя" required>
                                @foreach($usersList as $userOption)
                                    <option value="{{ $userOption->id }}" @if($userOption->id == $item->user_id) selected @endif>
                                        {{ $userOption->id_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="description">Описание</label>
                            <textarea name="description" class="form-control" id="description" rows="3">{{ old('description', $item->description) }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
