@extends('layouts.app')

@section('content')
<div class="container">
    @php /** @var App\Models\Book $item */ @endphp
        @if($item->exists)
            <form method="POST" action="{{ route('admin.books.model.update', $item->id) }}">
                @method('PATCH')
        @else
            <form method="POST" action="{{ route('admin.books.model.store') }}">
        @endif

            @csrf
            @php /** @var \Illuminate\Support\ViewErrorBag $errors */ @endphp
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ $errors->first() }}
                </div>
            @endif
            @if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session()->get('success') }}
                </div>
            @endif
            <div class="row justify-content-center">
                <div class="col-lg-8 col-md-8">
                    @include('admin.books.includes.item_edit_main_col')
                </div>
                <div class="col-lg-4 col-md-4">
                    @include('admin.books.includes.item_edit_add_col')
                </div>
            </div>
        </form>
</div>
@endsection
