@extends('layouts.admin')

@section('title', 'Главная')

@section('content')

    @include('admin.home.books.includes.result_messages')

    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-body p-0">
                <table class="table table-striped projects">
                    <thead>
                    <tr>
                        <th>
                            #
                        </th>
                        <th>
                            Название
                        </th>
                        <th>
                            Управление
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($paginator as $item)
                        @php /** @var App\Models\Book $item */ @endphp
                        @php /** @var Illuminate\Pagination\Paginator $paginator */ @endphp
                        <tr>
                            <td>
                                #
                            </td>
                            <td>
                                <a>
                                    {{ $item->title }}
                                </a>
                                <br/>
                                <small>
                                    {{ $item->created_at }}
                                </small>
                            </td>
                            <td class="project-actions text-right">
                                <a class="btn btn-primary btn-sm" href="#">
                                    <i class="fas fa-folder">
                                    </i>
                                    View
                                </a>
                                <a class="btn btn-info btn-sm" href="#">
                                    <i class="fas fa-pencil-alt">
                                    </i>
                                    Edit
                                </a>
                                <a class="btn btn-danger btn-sm" href="#">
                                    <i class="fas fa-trash">
                                    </i>
                                    Delete
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->

        @if($paginator->total() > $paginator->count())
            <br>
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            {{ $paginator->links() }}
                        </div>
                    </div>
                </div>
            </div>
        @endif

    </section>
@endsection
