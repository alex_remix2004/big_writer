@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-4 col-md-4">
                <div class="card">
                    <div class="card-header">
                        Фундаментальные (Fundamental)
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <a href="{{ route('propertyContainer') }}">Контейнер свойств (Property Container)</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('delegation') }}">Делегирование (Delegation)</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('eventChannel') }}">Канал событий (Event channel)</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('interfacePattern') }}">Интерфейс (Interface)</a>
                        </li>
                    </ul>
                </div>
                <br>
                <div class="card">
                    <div class="card-header">
                        Порождающие шаблоны (Creational)
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <a href="{{ route('abstractFactory') }}">Абстрактная фабрика (Abstract factory)</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('factoryMethod') }}">Фабричный метод (Factory Method)</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('staticFactory') }}">Статическая фабрика (Static factory)</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('simpleFactory') }}">Простая фабрика (Simple factory)</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('singleton') }}">Одиночка (Singleton)</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('multiton') }}">Пул одиночек (Multiton)</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('builder') }}">Строитель (Builder)</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('lazyInitialization') }}">Ленивая загрузка (Lazy initialization (load))</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('prototype') }}">Прототип, клон (prototype, clone)</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('objectPool') }}">Объектный пул (Object pool)</a>
                        </li>
                    </ul>
                </div>
                <br>
                <div class="card">
                    <div class="card-header">
                        Поведенческие шаблоны (Bihavioral)
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <a href="{{ route('strategy') }}">Стратегия (strategy)</a>
                        </li>
                    </ul>
                </div>
                <br>
                <div class="card">
                    <div class="card-header">
                        Структурные шаблоны (Structural)
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <a href="{{ route('adapter') }}">Адаптер (Adapter / Wrapper)</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('facade') }}">Фасад (Facade)</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-8 col-md-8">
                <div class="card">
                    <div class="card-body">
                        {{ $name }}
                    </div>
                </div>
                <br>
                <div class="card">
                    <div class="card-body">
                        {{ $description }}
                    </div>
                </div>
                <br>
                <div class="card">
                    <div class="card-body">
                        <pre>{{ print_r($item) }}</pre>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
