<?php

use App\Http\Controllers\BehavioralPatternsController;
use App\Http\Controllers\CreationalPatternsController;
use App\Http\Controllers\DiggingDeeperController;
use App\Http\Controllers\StructuralPatternsController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RestController;
use App\Http\Controllers\FundamentalPatternsControllerController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::resource('rest', RestController::class)->names('rest');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::middleware(['role:admin'])->prefix('super-admin')->group( function () {
    Route::get('/', [App\Http\Controllers\Admin\HomeController::class, 'index'])->name('admin.home.index');
    Route::resource('book', App\Http\Controllers\Admin\BookController::class)->names('admin.home.book');
});

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'fundamentals'], function() {
    Route::get('property-container', [FundamentalPatternsControllerController::class, 'propertyContainer'])->name('propertyContainer');
    Route::get('delegation', [FundamentalPatternsControllerController::class, 'delegation'])->name('delegation');
    Route::get('event-channel', [FundamentalPatternsControllerController::class, 'eventChannel'])->name('eventChannel');
    Route::get('interface-pattern', [FundamentalPatternsControllerController::class, 'interfacePattern'])->name('interfacePattern');
});

Route::group(['prefix' => 'creational'], function() {
    Route::get('abstract-factory', [CreationalPatternsController::class, 'abstractFactory'])->name('abstractFactory');
    Route::get('factory-method', [CreationalPatternsController::class, 'factoryMethod'])->name('factoryMethod');
    Route::get('static-factory', [CreationalPatternsController::class, 'staticFactory'])->name('staticFactory');
    Route::get('simple-factory', [CreationalPatternsController::class, 'simpleFactory'])->name('simpleFactory');
    Route::get('singleton', [CreationalPatternsController::class, 'singleton'])->name('singleton');
    Route::get('multiton', [CreationalPatternsController::class, 'multiton'])->name('multiton');
    Route::get('builder', [CreationalPatternsController::class, 'builder'])->name('builder');
    Route::get('lazy-initialization', [CreationalPatternsController::class, 'lazyInitialization'])->name('lazyInitialization');
    Route::get('prototype', [CreationalPatternsController::class, 'prototype'])->name('prototype');
    Route::get('object-pool', [CreationalPatternsController::class, 'objectPool'])->name('objectPool');
});
Route::group(['prefix' => 'structural'], function() {
    Route::get('adapter', [StructuralPatternsController::class, 'adapter'])->name('adapter');
    Route::get('facade', [StructuralPatternsController::class, 'facade'])->name('facade');
});

Route::group(['prefix' => 'behavioral'], function() {
    Route::get('strategy', [BehavioralPatternsController::class, 'strategy'])->name('strategy');
});

Route::group(['prefix' => 'digging_deeper'], function() {
    Route::get('collections', [DiggingDeeperController::class, 'collections'])->name('digging_deeper.collections');
    Route::get('prepare-catalog', [DiggingDeeperController::class, 'prepareCatalog'])->name('digging_deeper.prepareCatalog');
});

Route::group(['namespace' => 'App\Http\Controllers\Book', 'prefix' => 'books'], function() {
    Route::resource('index', 'BookController');
});

Route::get('admin/books/pages/{page}/restore', [App\Http\Controllers\Book\Admin\PageController::class, 'restore'])->name('admin.pages.model.restore');
Route::group(['namespace' => 'App\Http\Controllers\Book\Admin', 'prefix' => 'admin/books'], function() {
    $methods = ['index', 'show', 'edit', 'update', 'create', 'store', 'destroy'];
    Route::resource('item', 'BookController')
        ->only($methods)
        ->names('admin.books.model');

    Route::resource('pages', 'PageController')
        ->except(['show'])
        ->names('admin.pages.model');
});
