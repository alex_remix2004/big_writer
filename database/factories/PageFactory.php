<?php

namespace Database\Factories;

use App\Models\Page;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Page::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->sentence(rand(3, 8), true);
        $txt = $this->faker->realText(rand(1000, 4000));
        $created_at = $this->faker->dateTimeBetween('-3 months', '-1 months');

        return [
            'title' => $title,
            'content_raw' => $txt,
            'content_html' => $txt,
            'slug' => Str::of($title)->slug(),
            'description' => $this->faker->text(rand(40, 100)),
            'book_id' => rand(1, 30),
            'created_at' => $created_at,
            'updated_at' => $created_at,
        ];
    }
}
