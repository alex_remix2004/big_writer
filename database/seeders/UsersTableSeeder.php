<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $created_at_1 = $faker->dateTimeBetween('-4 months', '- 3 months');
        $created_at_2 = $faker->dateTimeBetween('-4 months', '- 3 months');

        $data = [
            [
                'name' => 'Alex',
                'email' => 'alex@alex.aa',
                'password' => bcrypt('123456'),
                'created_at' => $created_at_1,
                'updated_at' => $created_at_1,
            ],
            [
                'name' => 'Anna',
                'email' => 'anna@anna.aa',
                'password' => bcrypt('112233'),
                'created_at' => $created_at_2,
                'updated_at' => $created_at_2,
            ],
        ];

        \DB::table('users')->insert($data);
    }
}
