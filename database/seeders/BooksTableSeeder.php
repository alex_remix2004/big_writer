<?php

namespace Database\Seeders;

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $created_at = $faker->dateTimeBetween('-2 months', '- 1 months');

        $books = [];
        $title = 'Книга';

        $books[] = [
            'user_id' => 1,
            'title' => $title,
            'slug' => Str::of($title)->slug(),
            'description' => 'Это описание главной книги',
            'created_at' => $created_at,
            'updated_at' => $created_at,
        ];

        for ($i = 1; $i <= 30; $i++) {
            $created_at = $faker->dateTimeBetween('-2 months', '- 1 months');
            $title = 'Книга '.$i;
            $description = 'Это описание '.$i;
            $user_id = ($i > 4) ? rand(1, 2) : 1;

            $books[] = [
                'user_id' => $user_id,
                'title' => $title,
                'slug' => Str::of($title)->slug(),
                'description' => $description,
                'created_at' => $created_at,
                'updated_at' => $created_at,
            ];
        }

        \DB::table('books')->insert($books);
    }
}
