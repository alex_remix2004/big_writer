<?php

namespace App\Patterns\Creational\Multiton;

/**
 * Class SimpleMultitonNext
 * @package App\Patterns\Creational\Multiton
 */
class SimpleMultitonNext extends SimpleMultiton
{

    protected static $instances = [];

    /**
     * @var $test2
     */
    public $test2;

}
