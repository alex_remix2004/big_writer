<?php

namespace App\Patterns\Creational\Multiton;


use App\Patterns\Creational\Multiton\Interfaces\MultitonInterface;

trait MultitonTrait
{
    protected static $instances = [];
    private $name;

    /**
     * Запрещаем прямое создание.
     */
    private function __construct()
    {
        //
    }

    /**
     * Запрещаем клонирование.
     */
    private function __clone()
    {
        //
    }

    /**
     * Запрещаем десереализацию.
     */
    private function __wakeup()
    {
        //
    }

    /**
     * @param string $instanceName
     * @return MultitonInterface
     */
    public static function getInstance(string $instanceName): MultitonInterface
    {
        if (isset(static::$instances[$instanceName])) {
            return static::$instances[$instanceName];
        }

        static::$instances[$instanceName] = new static();
        static::$instances[$instanceName]->setName($instanceName);

        return static::$instances[$instanceName];
    }

    /**
     * @param $value
     * @return $this
     */
    public function setName($value)
    {
        $this->name = $value;

        return $this;
    }
}
