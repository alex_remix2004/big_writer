<?php

namespace App\Patterns\Creational\Multiton;

use App\Patterns\Creational\Multiton\Interfaces\MultitonInterface;

class SimpleMultiton implements MultitonInterface
{
    use MultitonTrait;

    private $test;

    public function setTest($value)
    {
        return $this->test = $value;
    }

    public static function getDescription()
    {
        return "Мультитон (англ. multiton) — порождающий шаблон проектирования, который обобщает шаблон \"Одиночка\".\n
        В то время, как \"Одиночка\"разрешает создание лишь одного экземпляра класса, мультитон позволяет создавать несколько экземпляров,\n
        которые управляются через ассоциативный массив. Создаётся лишь один экземпляр для каждого из ключей ассоциативного массива,\n
        что позволяет контролировать уникальность объекта по какому-либо признаку.";
    }
}
