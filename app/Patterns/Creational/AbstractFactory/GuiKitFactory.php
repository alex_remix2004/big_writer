<?php

namespace App\Patterns\Creational\AbstractFactory;


use App\Patterns\Creational\AbstractFactory\Factories\BootstrapFactory;
use App\Patterns\Creational\AbstractFactory\Factories\SemanticUiFactory;
use App\Patterns\Creational\AbstractFactory\Interfaces\GuiKitFactoryInterface;
use Exception;

class GuiKitFactory
{
    /**
     * @return string
     */
    public static function getDescription()
    {
        return "Абстрактная фабрика (англ. Abstract factory) — порождающий шаблон проектирования, предоставляет интерфейс для создания семейств\n
        взаимосвязанных или взаимозависимых объектов, не специфицируя их конкретных классов. Шаблон реализуется созданием абстрактного класса Factory,\n
        который представляет собой интерфейс для создания компонентов системы (например, для оконного интерфейса он может создавать окна и кнопки).\n\n
        Затем пишутся классы, реализующие этот интерфейс";
    }

    /**
     * @param $type
     * @return GuiKitFactoryInterface
     * @throws Exception
     */
    public function getFactory($type): GuiKitFactoryInterface
    {
        switch ($type) {
            case 'bootstrap';
                $factory = new BootstrapFactory();
                break;

            case 'semanticui';
                $factory = new SemanticUiFactory();
                break;

            default:
                throw new Exception("Неизвестный тип фабрики [{$type}]");
        }

        return $factory;
    }
}
