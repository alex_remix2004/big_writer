<?php

namespace App\Patterns\Creational\AbstractFactory\Classes;

use App\Patterns\Creational\AbstractFactory\Interfaces\CheckBoxInterface;

class CheckBoxBootstrap implements CheckBoxInterface
{
    public function draw()
    {
        return __CLASS__;
    }
}
