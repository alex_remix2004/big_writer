<?php

namespace App\Patterns\Creational\AbstractFactory\Interfaces;

interface GuiKitFactoryInterface
{
    /**
     * @return ButtonInterface
     */
    public function buildButton(): ButtonInterface;

    /**
     * @return CheckBoxInterface
     */
    public function buildCheckBox(): CheckBoxInterface;
}
