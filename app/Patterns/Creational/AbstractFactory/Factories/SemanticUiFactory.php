<?php

namespace App\Patterns\Creational\AbstractFactory\Factories;


use App\Patterns\Creational\AbstractFactory\Classes\ButtonSemanticUi;
use App\Patterns\Creational\AbstractFactory\Classes\CheckBoxSemanticUi;
use App\Patterns\Creational\AbstractFactory\Interfaces\ButtonInterface;
use App\Patterns\Creational\AbstractFactory\Interfaces\CheckBoxInterface;
use App\Patterns\Creational\AbstractFactory\Interfaces\GuiKitFactoryInterface;

class SemanticUiFactory implements GuiKitFactoryInterface
{
    /**
     * @return ButtonInterface
     */
    public function buildButton(): ButtonInterface
    {
        return new ButtonSemanticUi();
    }

    /**
     * @return CheckBoxInterface
     */
    public function buildCheckBox(): CheckBoxInterface
    {
        return new CheckBoxSemanticUi();
    }
}
