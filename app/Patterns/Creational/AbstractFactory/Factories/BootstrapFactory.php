<?php

namespace App\Patterns\Creational\AbstractFactory\Factories;


use App\Patterns\Creational\AbstractFactory\Classes\ButtonBootstrap;
use App\Patterns\Creational\AbstractFactory\Classes\CheckBoxBootstrap;
use App\Patterns\Creational\AbstractFactory\Interfaces\ButtonInterface;
use App\Patterns\Creational\AbstractFactory\Interfaces\CheckBoxInterface;
use App\Patterns\Creational\AbstractFactory\Interfaces\GuiKitFactoryInterface;

class BootstrapFactory implements GuiKitFactoryInterface
{
    /**
     * @return ButtonInterface
     */
    public function buildButton(): ButtonInterface
    {
        return new ButtonBootstrap();
    }

    /**
     * @return CheckBoxInterface
     */
    public function buildCheckBox(): CheckBoxInterface
    {
        return new CheckBoxBootstrap();
    }
}
