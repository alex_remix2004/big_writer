<?php

namespace App\Patterns\Creational\Builder\Classes;


class Page
{
    public $title;
    public $body;
    public $tags = [];
    public $categories = [];

    /**
     * Page constructor.
     * @param string $title
     * @param string $body
     * @param array $tags
     * @param array $categories
     *
     * данный подход не очень, т.к. возможно придется присваивать пустые атрибуты
     */
    /*public function __construct(string $title, string $body, array $categories = [], array $tags = [])
    {
        $this->title = $title;
        $this->body = $body;
        $this->categories = $categories;
        $this->tags = $tags;
    }*/
}
