<?php

namespace App\Patterns\Creational\Builder;


use App\Patterns\Creational\Builder\Classes\Page;
use App\Patterns\Creational\Builder\Interfaces\PageBuilderInterface;

class PageManager
{
    private $builder;

    /**
     * @param PageBuilderInterface $builder
     * @return $this
     */
    public function setBuilder(PageBuilderInterface $builder)
    {
        $this->builder = $builder;

        return $this;
    }

    /**
     * @return Page
     */
    public function createCleanPage()
    {
        return $this->builder->getPage();
    }

    /**
     * @return Page
     */
    public function createNewPageIt()
    {
        return $this->builder
            ->setTitle('New page about IT')
            ->setBody('This is new page about IT..')
            ->setCategories(['category_it'])
            ->setTags(['page_it', 'new_page'])
            ->getPage();
    }

    /**
     * @return Page
     */
    public function createNewPageCats()
    {
        return $this->builder
            ->setTitle('New page about cats')
            ->setCategories(['category_cats', 'category_pets'])
            ->setTags(['tag_cats', 'tag_pets'])
            ->getPage();
    }

    /**
     * @return string
     */
    public static function getDescription()
    {
        return "Отделяет конструирование сложного объекта от его представления так,\n
        что в результате одного и того же процесса конструирования могут получаться разные представления.";
    }
}
