<?php

namespace App\Patterns\Creational\Builder\Interfaces;


use App\Patterns\Creational\Builder\Classes\Page;

interface PageBuilderInterface
{
    /**
     * @return PageBuilderInterface
     */
    public function create(): PageBuilderInterface;

    /**
     * @param string $value
     * @return PageBuilderInterface
     */
    public function setTitle(string $value): PageBuilderInterface;

    /**
     * @param string $value
     * @return PageBuilderInterface
     */
    public function setBody(string $value): PageBuilderInterface;

    /**
     * @param array $value
     * @return PageBuilderInterface
     */
    public function setCategories(array $value): PageBuilderInterface;

    /**
     * @param array $value
     * @return PageBuilderInterface
     */
    public function setTags(array $value): PageBuilderInterface;

    /**
     * @return Page
     */
    public function getPage(): Page;
}
