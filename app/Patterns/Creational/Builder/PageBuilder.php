<?php

namespace App\Patterns\Creational\Builder;


use App\Patterns\Creational\Builder\Classes\Page;
use App\Patterns\Creational\Builder\Interfaces\PageBuilderInterface;

class PageBuilder implements PageBuilderInterface
{
    private $page;

    public function __construct()
    {
        $this->create();
    }

    /**
     * @return PageBuilderInterface
     */
    public function create(): PageBuilderInterface
    {
        $this->page = new Page();

        return $this;
    }

    /**
     * @param string $value
     * @return PageBuilderInterface
     */
    public function setTitle(string $value): PageBuilderInterface
    {
        $this->page->title = $value;

        return $this;
    }

    /**
     * @param string $value
     * @return PageBuilderInterface
     */
    public function setBody(string $value): PageBuilderInterface
    {
        $this->page->body = $value;

        return $this;
    }

    /**
     * @param array $value
     * @return PageBuilderInterface
     */
    public function setCategories(array $value): PageBuilderInterface
    {
        $this->page->categories = $value;

        return $this;
    }

    /**
     * @param array $value
     * @return PageBuilderInterface
     */
    public function setTags(array $value): PageBuilderInterface
    {
        $this->page->tags = $value;

        return $this;
    }

    /**
     * @return Page
     */
    public function getPage(): Page
    {
        $result = $this->page;
        $this->create();

        return $result;
    }
}
