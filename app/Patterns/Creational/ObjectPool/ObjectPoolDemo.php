<?php

namespace App\Patterns\Creational\ObjectPool;

use App\Patterns\Creational\ObjectPool\Objects\Calculator;
use App\Patterns\Creational\ObjectPool\Objects\CreditCard;
use App\Patterns\Creational\ObjectPool\Objects\User;

class ObjectPoolDemo
{
    /**
     * @var ObjectPool
     */
    private $objectPool;

    /**
     * ObjectPoolDemo constructor.
     */
    public function __construct()
    {
        $this->objectPool = ObjectPool::getInstance();

        $user = new User();
        $creditCard = new CreditCard();
        $calculator = new Calculator();

        $this->objectPool
            ->addObject($user)
            ->addObject($creditCard)
            ->addObject($calculator);
    }

    public function run()
    {
        //dd(__METHOD__, 1, $this->objectPool);

        $creditCard = $this->objectPool->getObject(CreditCard::class);
        $creditCard->cardId = '1111 2222 3333 4444';
        $creditCard->cardHolder = 'Card Holder';
        $creditCard->cardPwd = '123';

        $user = $this->objectPool->getObject(User::class);
        $user->name = 'User Name';

        $user2 = $this->objectPool->getObject(User::class);

        //dd(__METHOD__, 2, $this->objectPool, [$user, $user2, $creditCard]);

        $this->objectPool->release($creditCard);
        $this->objectPool->release($user);

        //dd(__METHOD__, 3, $this->objectPool, [$user, $user2, $creditCard]);

        $creditCard2 = $this->objectPool->getObject(CreditCard::class);
        $creditCard2->cardId = '5555 6666 7777 8888';
        $creditCard2->cardHolder = 'SOMEBODY HOLDER X';
        $creditCard2->cardPwd = '456';

        dd(__METHOD__, 4, $this->objectPool, [$creditCard, $user, $creditCard2]);
    }

    public static function getDescription()
    {
        return "Объектный пул (англ. object pool) — порождающий шаблон проектирования,\n
        набор инициализированных и готовых к использованию объектов. Когда системе требуется объект, он не создаётся,\n
        а берётся из пула. Когда объект больше не нужен, он не уничтожается, а возвращается в пул.";
    }
}
