<?php

namespace App\Patterns\Creational\ObjectPool;


use App\Patterns\Creational\ObjectPool\Interfaces\ObjectPullableInterface;
use App\Patterns\Creational\Singleton\SingletonTrait;
use Exception;

class ObjectPool
{
    use SingletonTrait;

    private $clones = [];
    private $prototypes = [];

    /**
     * @param ObjectPullableInterface $object
     * @return $this
     * @throws Exception
     */
    public function addObject(ObjectPullableInterface $object)
    {
        $key = $this->getObjectKey($object);
        $this->prototypes[$key] = $object;

        return $this;
    }

    /**
     * @param object|string $object
     * @return string
     * @throws Exception
     */
    private function getObjectKey($object)
    {
        if (is_object($object)) {
            $key = get_class($object);
        } elseif(is_string($object)) {
            $key = $object;
        } else {
            throw new Exception('???');
        }

        return $key;
    }

    /**
     * @param string $className
     * @return bool|null
     * @throws Exception
     */
    public function getObject(string $className)
    {
        $key = $this->getObjectKey($className);

        if (isset($this->clones[$key])) {
            return false;
        }

        if (empty($this->prototypes[$key])) {
            return null;
        }

        $this->clones[$key] = clone $this->prototypes[$key];

        return $this->clones[$key];
    }

    /**
     * @param ObjectPullableInterface $object
     * @throws Exception
     */
    public function release(ObjectPullableInterface $object)
    {
        $key = $this->getObjectKey($object);
        unset($this->clones[$key]);
        $object = null;
    }
}
