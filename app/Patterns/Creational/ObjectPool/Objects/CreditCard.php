<?php

namespace App\Patterns\Creational\ObjectPool\Objects;


use App\Patterns\Creational\ObjectPool\Interfaces\ObjectPullableInterface;

class CreditCard implements ObjectPullableInterface
{
    public function __clone()
    {

    }
}
