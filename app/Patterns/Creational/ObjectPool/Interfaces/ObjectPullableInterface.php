<?php

namespace App\Patterns\Creational\ObjectPool\Interfaces;

interface ObjectPullableInterface
{
    public function __clone();
}
