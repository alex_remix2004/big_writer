<?php

namespace App\Patterns\Creational\Prototype;


use Carbon\Carbon;

class PrototypeDemo
{
    public function run()
    {
        $client = new Client(2, 'Client');

        $deliveryDt = Carbon::parse('31.12.2030 15:00:00');
        $order = new Order(11, $deliveryDt, $client);

        $client->addOrder($order);

        $cloneOrder = clone $order;
        $cloneOrder->deliveryDt->addDay();

        return compact('client', 'order', 'cloneOrder');
    }

    public static function getDescription()
    {
        return "Задаёт виды создаваемых объектов с помощью экземпляра-прототипа и создаёт новые объекты путём копирования этого прототипа.\n
        Он позволяет уйти от реализации и позволяет следовать принципу «программирование через интерфейсы».\n
        В качестве возвращающего типа указывается интерфейс/абстрактный класс на вершине иерархии,\n
        а классы-наследники могут подставить туда наследника, реализующего этот тип.\n\n
        Проще говоря, это паттерн создания объекта через клонирование другого объекта вместо создания через конструктор.";
    }
}
