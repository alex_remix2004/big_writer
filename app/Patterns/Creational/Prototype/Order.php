<?php

namespace App\Patterns\Creational\Prototype;


use Carbon\Carbon;

class Order
{
    public $id;
    public $deliveryDt;
    private $client;

    /**
     * Order constructor.
     * @param $id
     * @param Carbon $deliveryDt
     * @param Client $client
     */
    public function __construct($id, Carbon $deliveryDt, Client $client)
    {
        $this->id = (string)$id;
        $this->deliveryDt = $deliveryDt;
        $this->client = $client;
    }

    /*
     * в данном месте решается проблема конфликтов клонируемых объектов ранее
     */
    public function __clone()
    {
        $this->id = $this->id . '_copy';
        $this->deliveryDt = $this->deliveryDt->copy();

        $this->client->addOrder($this);
    }
}
