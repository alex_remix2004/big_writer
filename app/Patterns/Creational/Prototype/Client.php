<?php

namespace App\Patterns\Creational\Prototype;


class Client
{
    public $id;
    public $name;
    private $orders;

    /**
     * Client constructor.
     * @param $id
     * @param $name
     */
    public function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @param Order $order
     */
    public function addOrder(Order $order)
    {
        $this->orders[$order->id] = $order;
    }
}
