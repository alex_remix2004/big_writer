<?php


namespace App\Patterns\Creational\FactoryMethod\Interfaces;


interface FormInterface
{
    public function render();
}
