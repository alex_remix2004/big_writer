<?php

namespace App\Patterns\Creational\FactoryMethod\Classes\Forms;

use App\Patterns\Creational\AbstractFactory\Factories\SemanticUiFactory;
use App\Patterns\Creational\AbstractFactory\Interfaces\GuiKitFactoryInterface;

class SemanticUiDialogForm extends AbstractForm
{
    /**
     * @return GuiKitFactoryInterface
     */
    function createGuiKit(): GuiKitFactoryInterface
    {
        return new SemanticUiFactory();
    }
}
