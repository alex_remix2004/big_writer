<?php

namespace App\Patterns\Creational\FactoryMethod\Classes\Forms;

use App\Patterns\Creational\AbstractFactory\Factories\BootstrapFactory;
use App\Patterns\Creational\AbstractFactory\Interfaces\GuiKitFactoryInterface;

class BootstrapDialogForm extends AbstractForm
{
    /**
     * @return GuiKitFactoryInterface
     */
    function createGuiKit(): GuiKitFactoryInterface
    {
        return new BootstrapFactory();
    }
}
