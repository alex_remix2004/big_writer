<?php

namespace App\Patterns\Creational\FactoryMethod\Classes\Forms;

use App\Patterns\Creational\AbstractFactory\Interfaces\GuiKitFactoryInterface;
use App\Patterns\Creational\FactoryMethod\Interfaces\FormInterface;

/**]
 * Class AbstractForm
 * @package App\Patterns\Creational\FactoryMethod\Classes\Forms
 */
abstract class AbstractForm implements FormInterface
{
    /**
     * @return array
     */
    public function render(): array
    {
        $guiKit = $this->createGuiKit();
        $result[] = $guiKit->buildCheckBox()->draw();
        $result[] = $guiKit->buildButton()->draw();

        return $result;
    }

    /**
     * @return GuiKitFactoryInterface
     */
    abstract function createGuiKit(): GuiKitFactoryInterface;
}
