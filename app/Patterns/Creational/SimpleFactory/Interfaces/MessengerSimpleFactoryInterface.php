<?php

namespace App\Patterns\Creational\SimpleFactory\Interfaces;

interface MessengerSimpleFactoryInterface
{
    public static function build($type);
}
