<?php

namespace App\Patterns\Creational\SimpleFactory;

use App\Patterns\Creational\SimpleFactory\Interfaces\MessengerSimpleFactoryInterface;
use App\Patterns\Fundametals\Delegation\Interfaces\MessengerInterface;
use App\Patterns\Fundametals\Delegation\Messengers\EmailMessenger;
use App\Patterns\Fundametals\Delegation\Messengers\SmsMessenger;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use PHPUnit\Util\Exception;

class MessengerSimpleFactory implements MessengerSimpleFactoryInterface
{
    /**
     * @param $type
     * @return MessengerInterface
     */
    public static function build($type): MessengerInterface
    {
        switch ($type) {
            case 'email':
                $messenger = new EmailMessenger();
                $messenger->setSender('admin@site.ru')->setMessage('Default EMAIL message');
                break;

            case 'sms':
                $messenger = new SmsMessenger();
                $messenger->setSender('88000005520')->setMessage('Default SMS message');
                break;

            default:
                throw new Exception("Неизвестнывй тип [{$type}]");
        }

        return $messenger;
    }

    /**
     * @return string
     */
    public static function getDescription()
    {
        return "SimpleFactory в примере ниже, это паттерн «Простая Фабрика».\n\n
        Она отличается от Статической Фабрики тем, что собственно не является статической.\n
        Таким образом, вы можете иметь множество фабрик с разными параметрами. Простая фабрика всегда должна быть предпочтительнее Статической фабрики!";
    }
}
