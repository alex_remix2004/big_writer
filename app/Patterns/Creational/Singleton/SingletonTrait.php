<?php

namespace App\Patterns\Creational\Singleton;

trait SingletonTrait
{
    private static $instance = null;

    /**
     * запрещаем прямое создание
     * SingletonTrait constructor.
     */
    private function __construct()
    {
        //
    }

    /**
     * запрещаем клонирование
     */
    private function __clone()
    {
        //
    }

    /**
     * запрещаем десериализация
     */
    private function __wakeup()
    {
        //
    }

    /**
     * @return SingletonTrait
     */
    public static function getInstance()
    {
        return static::$instance ?? (static::$instance = new static());
    }
}
