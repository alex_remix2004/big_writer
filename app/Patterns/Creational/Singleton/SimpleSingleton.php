<?php

namespace App\Patterns\Creational\Singleton;

class SimpleSingleton
{
    private static $instance;
    private $test;

    /**
     * @return SimpleSingleton
     */
    public static function getInstance()
    {
        return static::$instance ?? (static::$instance = new static());
    }

    /**
     * @param $value
     */
    public function setTest($value)
    {
        $this->test = $value;
    }

    /**
     * @return string
     */
    public static function getDescription()
    {
        return "Одиночка (англ. Singleton) — порождающий шаблон проектирования, гарантирующий,\n
        что в однопоточном приложении будет единственный экземпляр некоторого класса, и предоставляющий глобальную точку доступа к этому экземпляру.";
    }
}
