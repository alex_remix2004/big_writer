<?php

namespace App\Patterns\Creational\Singleton;

use App\Patterns\Creational\Singleton\Contracts\AnotherConnection;

/**
 * Должен быть Singleton
 * Порождать только так - app(LaravelSingleton::class)
 *
 * Class LaravelSingleton
 * @package App\Patterns\Creational\Singleton
 */
class LaravelSingleton implements AnotherConnection
{
    private $test;

    /**
     * @param $value
     */
    public function setTest($value)
    {
        $this->test = $value;
    }
}
