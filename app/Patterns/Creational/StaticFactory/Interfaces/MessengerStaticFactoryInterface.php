<?php

namespace App\Patterns\Creational\StaticFactory\Interfaces;

interface MessengerStaticFactoryInterface
{
    public static function build();
}
