<?php

namespace App\Patterns\Creational\StaticFactory;


use App\Patterns\Creational\StaticFactory\Interfaces\MessengerStaticFactoryInterface;
use App\Patterns\Fundametals\Delegation\AppMessenger;
use App\Patterns\Fundametals\Delegation\Interfaces\MessengerInterface;
use PHPUnit\Util\Exception;

class StaticFactory implements MessengerStaticFactoryInterface
{
    /**
     * @param string $type
     * @return MessengerInterface
     */
    public static function build(string $type = 'email'): MessengerInterface
    {
        $messenger = new AppMessenger();

        switch ($type) {
            case 'email':
                $messenger->toEmail();
                $sender = 'admin@site.ru';
                break;

            case 'sms':
                $messenger->toSms();
                $sender = '88000005520';
                break;

            default:
                throw new Exception("Неизвестнывй тип [{$type}]");
        }

        return $messenger->setSender($sender)->setMessage('default message');
    }

    /**
     * @return string
     */
    public static function getDescription()
    {
        return "Подобно AbstractFactory, этот паттерн используется для создания ряда связанных или зависимых объектов.\n
        Разница между этим шаблоном и Абстрактной Фабрикой заключается в том, что Статическая Фабрика использует только один статический метод,\n
        чтобы создать все допустимые типы объектов.\n Этот метод, обычно, называется factory или build.";
    }
}
