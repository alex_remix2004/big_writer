<?php

namespace App\Patterns\Creational\LazyInitialization;


use App\Models\User;

class LazyInitialization
{
    /**
     * @var User|null
     */
    private $user = null;

    /**
     * LazyInitialization constructor.
     */
    public function __construct()
    {
        // $this->user = User::first(); # нет необходмости всегда инициализировать юзера, лучше делать выборочно через getUser() - lazy load
    }

    /**
     * @return User
     */
    public function getUser()
    {
        // return $this->user ?? ($this->user = User::first());

        if (is_null($this->user)) {
            $this->user = User::first();
            \Debugbar::info('Unit User');
        }

        return $this->user;
    }

    public function getData1()
    {

    }

    public function getAction1()
    {

    }

    public function setSomeData()
    {

    }

    public static function getDescription()
    {
        return "Отложенная (ленивая) инициализация (англ. Lazy initialization) — приём в программировании,\n
        когда некоторая ресурсоёмкая операция (создание объекта, вычисление значения) выполняется непосредственно перед тем,\n
        как будет использован её результат. Таким образом, инициализация выполняется «по требованию», а не заблаговременно.\n
        Аналогичная идея находит применение в самых разных областях: например, компиляция «на лету» и логистическая концепция «Точно в срок».\n\n
        Частный случай ленивой инициализации — создание объекта в момент обращения к нему — является одним из порождающих шаблонов проектирования.\n
        Как правило, он используется в сочетании с такими шаблонами как Фабричный метод, Одиночка и Заместитель.";
    }
}
