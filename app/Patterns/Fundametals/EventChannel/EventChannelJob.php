<?php

namespace App\Patterns\Fundametals\EventChannel;


use App\Patterns\Fundametals\EventChannel\Classes\EventChannel;
use App\Patterns\Fundametals\EventChannel\Classes\Publisher;
use App\Patterns\Fundametals\EventChannel\Classes\Subscriber;

/**
 * Данный джоб не принадлежит Ларавель
 * Class EventChannelJob
 * @package App\Patterns\Fundametals\EventChannel
 */
class EventChannelJob
{
    public function run()
    {
        $newsChannel = new EventChannel();

        $highgardenGroup = new Publisher('highgarden-news', $newsChannel);
        $winterfellNews = new Publisher('winterfell-news', $newsChannel);
        $winterfellDaily = new Publisher('winterfell-daily', $newsChannel);

        $sansa = new Subscriber('Sansa Stark');
        $arya = new Subscriber('Arya Stark');
        $cersei = new Subscriber('Cersei Lannister');
        $snow = new Subscriber('John Snow');

        $newsChannel->subscribe('highgarden-news', $cersei);
        $newsChannel->subscribe('winterfell-news', $sansa);
        $newsChannel->subscribe('highgarden-news', $arya);
        $newsChannel->subscribe('winterfell-news', $arya);
        $newsChannel->subscribe('winterfell-daily', $snow);

        $highgardenGroup->publish('New highgarden post');
        $winterfellNews->publish('New winterfell post');
        $winterfellDaily->publish('New alternative winterfell post');
    }

    /**
     * @return string
     */
    public static function getDescription(): string
    {
        return "Канал событий (англ. event channel) — фундаментальный шаблон проектирования, используется для создания канала связи и коммуникации через него посредством событий. Этот канал обеспечивает возможность разным издателям публиковать события и подписчикам, подписываясь на них, получать уведомления.\n\n
        Этот шаблон является расширением шаблона Издатель-подписчик (Publish/Subscribe) путем добавления функций, которые присущи распределенной среде. Так канал является централизованным и подписчик может получать опубликованные события от более, чем одного объекта, даже если он зарегистрирован только на одном канале.\n\n
        В общем случае шаблон Канал событий описывает интерфейс для объектов-представителей для подписки на канал событий и для объектов-представителей для публикации событий в рамках канала. Использование неких представителей позволяет взаимодействовать настоящим издателям и подписчикам вне рамках самого канала, но посредством него.";
    }
}
