<?php

namespace App\Patterns\Fundametals\EventChannel\Classes;

use App\Patterns\Fundametals\EventChannel\Interfaces\SubscriberInterface;

/**
 * Class Subscriber
 * @package App\Patterns\Fundametals\EventChannel\Classes
 */
class Subscriber implements SubscriberInterface
{
    /**
     * @var string
     */
    private $name;

    /**
     * Subscriber constructor.
     * @param $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @param $data
     * @return mixed|void
     */
    public function notify($data)
    {
        $message = "{$this->getName()} оповещен(а) данными [{$data}]";

        \Debugbar::info($message);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
