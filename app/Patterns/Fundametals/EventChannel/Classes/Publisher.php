<?php

namespace App\Patterns\Fundametals\EventChannel\Classes;

use App\Patterns\Fundametals\EventChannel\Interfaces\EventChannelInterface;
use App\Patterns\Fundametals\EventChannel\Interfaces\PublisherInterface;

/**
 * Class Publisher
 * @package App\Patterns\Fundametals\EventChannel\Classes
 */
class Publisher implements PublisherInterface
{
    private $topic;
    private $eventChannel;

    /**
     * Publisher constructor.
     * @param $topic
     * @param EventChannelInterface $eventChannel
     */
    public function __construct($topic, EventChannelInterface $eventChannel)
    {
        $this->topic = $topic;
        $this->eventChannel = $eventChannel;
    }

    /**
     * @param $data
     * @return mixed|void
     */
    public function publish($data)
    {
        $this->eventChannel->publish($this->topic, $data);
    }
}
