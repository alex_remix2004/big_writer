<?php

namespace App\Patterns\Fundametals\EventChannel\Classes;

use App\Patterns\Fundametals\EventChannel\Interfaces\EventChannelInterface;
use App\Patterns\Fundametals\EventChannel\Interfaces\SubscriberInterface;

class EventChannel implements EventChannelInterface
{
    private $topics = [];

    /**
     * @param $topic
     * @param $data
     */
    public function publish($topic, $data)
    {
        if (empty($this->topics[$topic])) {
            return;
        }

        foreach ($this->topics[$topic] as $subscriber) {
            /** @var SubscriberInterface $subscriber */
            $subscriber->notify($data);
        }
    }

    /**
     * @param $topic
     * @param SubscriberInterface $subscriber
     */
    public function subscribe($topic, SubscriberInterface $subscriber)
    {
        $this->topics[$topic][] = $subscriber;

        $message = "{$subscriber->getName()} подписан(а) на топик [{$topic}]";

        return \Debugbar::debug($message);
    }
}
