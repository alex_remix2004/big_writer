<?php

namespace App\Patterns\Fundametals\EventChannel\Interfaces;

/**
 * Interface PublisherInterface
 * @package App\Patterns\Fundametals\EventChannel\Interfaces
 */
interface PublisherInterface
{
    /**
     * Уведомление подписчиков
     * @param $data
     * @return mixed
     */
    public function publish($data);
}
