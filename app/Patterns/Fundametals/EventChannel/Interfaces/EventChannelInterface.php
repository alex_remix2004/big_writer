<?php

namespace App\Patterns\Fundametals\EventChannel\Interfaces;

/**
 * Связующее звено между подписчиками и издателями
 * Interface EventChannelInterface
 * @package App\Patterns\Fundametals\EventChannel\Interfaces
 */
interface EventChannelInterface
{
    public function publish($topic, $data);
    public function subscribe($topic, SubscriberInterface $subscriber);
}
