<?php

namespace App\Patterns\Fundametals\EventChannel\Interfaces;

/**
 * Interface SubscriberInterface
 * @package App\Patterns\Fundametals\EventChannel\Interfaces
 */
interface SubscriberInterface
{
    /**
     * Уведомление подписчика
     * @param $data
     * @return mixed
     */
    public function notify($data);
}
