<?php

namespace App\Patterns\Fundametals\Delegation\Interfaces;

interface MessengerInterface
{
    /**
     * @param $value
     * @return mixed
     */
    public function setSender($value): MessengerInterface;

    /**
     * @param $value
     * @return mixed
     */
    public function setRecipient($value): MessengerInterface;

    /**
     * @param $value
     * @return mixed
     */
    public function setMessage($value): MessengerInterface;

    /**
     * @return bool
     */
    public function send(): bool;
}
