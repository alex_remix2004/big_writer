<?php

namespace App\Patterns\Fundametals\Delegation\Messengers;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class EmailMessenger extends AbstractMessenger
{
    use HasFactory;

    /**
     * Перегрузка метода send()
     * @return bool
     */
    public function send(): bool
    {
        \Debugbar::info('Sent by ' . __METHOD__);

        return parent::send();
    }
}
