<?php

namespace App\Patterns\Fundametals\PropertyContainer;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * В рабочем проекте унаследовать данный класс от PropertyContainer можно будет как инъекцию зависимостей
 * т.к. он уже будет наслодован от какого-либо класса (в текущем примере это через extends PropertyContainer)
 *
 * Class Page
 * @package App\Patterns\Fundametals\PropertyContainer
 */
class Page extends PropertyContainer
{
    use HasFactory;

    private $title;
    private $book_id;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
    /**
     * @return string
     */
    public function getBook()
    {
        return $this->book_id;
    }

    /**
     * @param $book_id
     */
    public function setBook($book_id)
    {
        $this->book_id = $book_id;
    }
}
