<?php

namespace App\Patterns\Fundametals\PropertyContainer;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Patterns\Fundametals\PropertyContainer\Interfaces\PropertyContainerInterface;

class PropertyContainer implements PropertyContainerInterface
{
    use HasFactory;

    private $propertyContainer = [];

    /**
     * @return string
     */
    public static function getDescription()
    {
        return "Классическим примером использованием шаблона является приложение, используемое для хранения и классификации информации. Например, приложение заказа фильмов[1]. При разработке класса, представляющего фильм, при разработке и запуске приложения невозможно предусмотреть все возможные свойства (атрибуты), описывающие фильм. Поэтому класс фильма при необходимости в любой момент может быть расширен дополнительными свойствами. Для этого требуются предусмотреть механизм расширения свойств перед выпуском приложения.\n\nКонтейнер свойств, предоставляет механизм для динамического расширения объектов дополнительными атрибутами во время выполнения. Кроме этого, приложению могут потребоваться ещё модули, которые явным образом используют преимущества нового свойства, если оно было добавлено.";
    }

    /**
     * @param $propertyName
     * @param $value
     */
    function addProperty($propertyName, $value)
    {
        $this->propertyContainer[$propertyName] = $value;
    }

    /**
     * @param $propertyName
     */
    function deleteProperty($propertyName)
    {
        unset($this->propertyContainer[$propertyName]);
    }

    /**
     * @param $propertyName
     * @return mixed|null
     */
    function getProperty($propertyName)
    {
        return $this->propertyContainer[$propertyName] ?? null;
    }

    /**
     * @param $propertyName
     * @param $value
     * @throws \Exception
     */
    function setProperty($propertyName, $value)
    {
        if (!isset($this->propertyContainer[$propertyName])) {
            throw new \Exception("Property name [{$propertyName}] not found");
        }

        $this->propertyContainer[$propertyName] = $value;
    }
}
