<?php

namespace App\Patterns\Structural\Adapter\Classes;


use App\Patterns\Structural\Adapter\Interfaces\MediaLibraryInterface;

class MediaLibrarySelfWritten implements MediaLibraryInterface
{
    /**
     * @param $pathToFile
     * @return string
     */
    public function upload($pathToFile): string
    {
        \Debugbar::info(__METHOD__);

        return md5(__METHOD__ . $pathToFile);
    }

    /**
     * @param $pathToFile
     * @return string
     */
    public function get($pathToFile): string
    {
        \Debugbar::info(__METHOD__);

        return '';
    }
}
