<?php

namespace App\Patterns\Structural\Adapter\Classes;


use App\Patterns\Structural\Adapter\Interfaces\MediaLibraryInterface;
use Exception;

class MediaLibraryAdapter implements MediaLibraryInterface
{
    private $adapterObj;

    /**
     * MediaLibraryAdapter constructor.
     * @var MediaLibraryThirdParty
     */
    public function __construct()
    {
        $this->adapterObj = new MediaLibraryThirdParty();
    }

    public function upload($pathToFile): string
    {
        return $this->adapterObj->addMedia($pathToFile);
    }

    public function get($pathToFile): string
    {
        return $this->adapterObj->getMedia($pathToFile);
    }

    /**
     * Данный метод в данный шаблон проектирования не входит
     * @param $name
     * @param $arguments
     * @return false|mixed
     * @throws Exception
     */
    public function __call($name, $arguments)
    {
        if (method_exists($this->adapterObj, $name)) {
            return call_user_func_array([$this->adapterObj, $name], $arguments);
        } else {
            throw new Exception("Метод {$name} не найден");
        }
    }

    public static function getDescription()
    {
        return "Адаптер (англ. Adapter) — структурный шаблон проектирования, предназначенный для организации использования функций объекта,\n
        недоступного для модификации, через специально созданный интерфейс. Другими словами — это структурный паттерн проектирования,\n
        который позволяет объектам с несовместимыми интерфейсами работать вместе.";
    }
}
