<?php

namespace App\Patterns\Structural\Adapter\Classes;


use App\Patterns\Structural\Adapter\Interfaces\MediaLibraryThirdPartyInterface;

/**
 * сторонняя библиотека - закрыта для модификаций
 * Class MediaLibraryThirdParty
 * @package App\Patterns\Structural\Adapter\Classes
 */
class MediaLibraryThirdParty implements MediaLibraryThirdPartyInterface
{
    /**
     * @param $pathToFile
     * @return string
     */
    public function addMedia($pathToFile): string
    {
        \Debugbar::info(__METHOD__);

        return '';
    }

    /**
     * @param $fileCode
     * @return string
     */
    public function getMedia($fileCode): string
    {
        \Debugbar::info(__METHOD__);

        return '';
    }

    public function newMethod()
    {
        \Debugbar::info(__METHOD__);
    }
}
