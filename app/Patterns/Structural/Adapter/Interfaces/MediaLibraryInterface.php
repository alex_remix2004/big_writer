<?php

namespace App\Patterns\Structural\Adapter\Interfaces;


interface MediaLibraryInterface
{
    /**
     * загрузка изображения
     * @param $pathToFile
     * @return string
     */
    public function upload($pathToFile): string;

    /**
     * получение файла по имени
     * @param $pathToFile
     * @return string
     */
    public function get($pathToFile): string;
}
