<?php

namespace App\Patterns\Structural\Adapter\Interfaces;


interface MediaLibraryThirdPartyInterface
{
    /**
     * загрузка изображения
     * @param $pathToFile
     * @return string
     */
    public function addMedia($pathToFile): string;

    /**
     * @param $fileCode
     * @return string
     */
    public function getMedia($fileCode): string;
}
