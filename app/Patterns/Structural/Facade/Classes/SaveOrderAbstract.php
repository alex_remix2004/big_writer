<?php

namespace App\Patterns\Structural\Facade\Classes;


use App\Patterns\Structural\Facade\Entities\Order;
use Debugbar;

abstract class SaveOrderAbstract
{
    protected $order;
    protected $data;

    public function __construct(Order $order, array $data)
    {
        $this->order = $order;
        $this->data = $data;
    }

    public function run()
    {
        Debugbar::info(static::class);
    }
}
