<?php

namespace App\Patterns\Structural\Facade;


use App\Patterns\Structural\Facade\Classes\OrderSaveDates;
use App\Patterns\Structural\Facade\Classes\OrderSaveProducts;
use App\Patterns\Structural\Facade\Classes\OrderSaveUsers;
use App\Patterns\Structural\Facade\Entities\Order;

class OrderSaveFacade
{
    /**
     * @param Order $order
     * @param array $data
     * @return Order
     */
    public function save(Order $order, array $data)
    {
        (new OrderSaveProducts($order, $data))->run();
        (new OrderSaveDates($order, $data))->run();
        (new OrderSaveUsers($order, $data))->run();

        return true;
    }

    public static function getDescription()
    {
        return "Шаблон фасад (англ. Facade) — структурный шаблон проектирования,\n
        позволяющий скрыть сложность системы путём сведения всех возможных внешних вызовов к одному объекту,\n
        делегирующему их соответствующим объектам системы.";
    }
}
