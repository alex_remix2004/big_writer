<?php

namespace App\Patterns\Behavioral\Strategy;


use App\Models\User;
use App\Patterns\Behavioral\Strategy\Interfaces\SalaryStrategyInterface;
use App\Patterns\Behavioral\Strategy\Strategies\AbstractStrategy;
use Illuminate\Support\Collection;

class SalaryManager
{
    private $salaryStrategy;
    private $period;
    private $users;

    public function __construct(array $period, Collection $users)
    {
        $this->period = $period;
        $this->users = $users;
    }

    public function handle()
    {
        $usersSalary = $this->calculateSalary();

        $this->saveSalary($usersSalary); # вынести функционал в отдельный класс, т.к. это другая задача

        return $usersSalary;
    }

    /**
     * @return Collection
     */
    private function calculateSalary(): Collection
    {
        return $this->users->map(
            function (User $user) {
                $strategy = $this->getStrategyByUser($user);
                $salary = $this->setCalculateStrategy($strategy)->calculateUserSalary($this->period, $user);
                $strategyName = $strategy->getName();
                $userId = $user->id;

                return compact('userId', 'salary', 'strategyName');
            }
        );
    }

    /**
     * @param Collection $userSalary
     * @return bool
     */
    private function saveSalary(Collection $userSalary)
    {
        return true;
    }

    /**
     * @param User $user
     * @return AbstractStrategy
     * @throws \Throwable
     */
    private function getStrategyByUser(User $user): SalaryStrategyInterface
    {
        $strategyName = $user->departmentName() . 'Strategy';
        $strategyClass = __NAMESPACE__ . '\\Strategies\\' . ucwords($strategyName);

        throw_if(!class_exists($strategyClass), \Exception::class, "Класс не существует [{$strategyClass}]");

        return new $strategyClass;
    }

    /**
     * @param SalaryStrategyInterface $strategy
     * @return $this
     */
    private function setCalculateStrategy(SalaryStrategyInterface $strategy)
    {
        $this->salaryStrategy = $strategy;

        return $this;
    }

    /**
     * @param $period
     * @param $user
     * @return int
     */
    private function calculateUserSalary($period, $user)
    {
        return $this->salaryStrategy->calc($period, $user);
    }

    /**
     * @return string
     */
    public static function getDescription()
    {
        return "Стратегия (англ. Strategy) — поведенческий шаблон проектирования, предназначенный для определения семейства алгоритмов,\n
        инкапсуляции каждого из них и обеспечения их взаимозаменяемости. Это позволяет выбирать алгоритм путём определения соответствующего класса.\n
        Шаблон Strategy позволяет менять выбранный алгоритм независимо от объектов-клиентов, которые его используют.";
    }

}
