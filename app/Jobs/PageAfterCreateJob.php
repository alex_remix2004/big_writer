<?php

namespace App\Jobs;

use App\Models\Page;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PageAfterCreateJob implements ShouldQueue
{
    # Dispatchable - трейт для запуска(самозапуск)
    # InteractsWithQueue - трейт для управления очередю
    # Queueable - трейт для логики работы самого джоба(конект, время выполнения, мидллвары)
    # SerializesModels - трейт для того чтоб очередь не сериализовала саму модель, а тока при выполнении, и помещает затем в payload
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $page;

    /**
     * Create a new job instance.
     *
     * @param Page $page
     */
    public function __construct(Page $page)
    {
        $this->page = $page;
    }

    /**
     * Execute the job.
     * Выполняется, когда просмотрщик задач возьмет на выполнение
     *
     * @return void
     */
    public function handle()
    {
        # Helper's method logs()
        logs()->info("Создана новая страница [{$this->page->id}]");
    }
}
