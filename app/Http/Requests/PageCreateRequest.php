<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PageCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:5|max:200|unique:pages',
            'slug' => 'max:200',
            'book_id' => 'required|integer|exists:books,id',
            'content_raw' => 'string|min:3|max:10000',
        ];
    }

    /**
     * @return string[]
     */
    public function attributes()
    {
        return [
            'title' => 'Название главы',
        ];
    }

    /**
     * @return string[]
     */
    public function messages()
    {
        return [
            'title.required' => 'Введите название главы',
            'book_id.required'  => 'Выберите книгу из списка',
            'content_raw.min'  => 'Миинимальная длина главы :min символа',
        ];
    }
}
