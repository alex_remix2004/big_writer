<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:5|max:200',
            'slug' => 'max:200',
            'user_id' => 'required|integer|exists:users,id',
            'description' => 'string|min:3|max:500',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Заголовок обязательное поле',
            'user_id.required'  => 'Автор обязательное поле',
            'description.min:3'  => 'Количество символов в описании не менее 3',
        ];
    }
}
