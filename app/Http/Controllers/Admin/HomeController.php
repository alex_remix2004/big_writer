<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $pages_count = Page::all()->count();

        return view('admin.home.index', ['pages_count' => $pages_count]);
    }
}
