<?php

namespace App\Http\Controllers;

use App\Patterns\Creational\Builder\PageBuilder;
use App\Patterns\Creational\Builder\PageManager;
use App\Patterns\Creational\FactoryMethod\Classes\Forms\SemanticUiDialogForm;
use App\Patterns\Creational\AbstractFactory\GuiKitFactory;
use App\Patterns\Creational\AbstractFactory\Interfaces\GuiKitFactoryInterface;
use App\Patterns\Creational\FactoryMethod\Classes\Forms\BootstrapDialogForm;
use App\Patterns\Creational\FactoryMethod\FactoryMethod;
use App\Patterns\Creational\LazyInitialization\LazyInitialization;
use App\Patterns\Creational\Multiton\SimpleMultiton;
use App\Patterns\Creational\Multiton\SimpleMultitonNext;
use App\Patterns\Creational\ObjectPool\ObjectPoolDemo;
use App\Patterns\Creational\Prototype\PrototypeDemo;
use App\Patterns\Creational\SimpleFactory\MessengerSimpleFactory;
use App\Patterns\Creational\Singleton\AdvancedSingleton;
use App\Patterns\Creational\Singleton\Contracts\AnotherConnection;
use App\Patterns\Creational\Singleton\LaravelSingleton;
use App\Patterns\Creational\Singleton\SimpleSingleton;
use App\Patterns\Creational\StaticFactory\StaticFactory;
use Debugbar;

class CreationalPatternsController extends Controller
{
    /**
     * @var GuiKitFactoryInterface
     */
    private $guiKit;

    /**
     * CreationalPatternsController constructor.
     */
    public function __construct()
    {
        //$this->guiKit = (new GuiKitFactory())->getFactory('bootstrap');
        $this->guiKit = (new GuiKitFactory())->getFactory('semanticui');
    }

    /**
     * @link http://bigwriter.local/creational/abstract-factory
     */
    public function AbstractFactory()
    {
        $name = 'Абстрактная фабрика (Abstract factory)';
        $description = GuiKitFactory::getDescription();

        $item[] = $this->guiKit->buildButton()->draw();
        $item[] = $this->guiKit->buildCheckBox()->draw();

        Debugbar::info($item);

        return view('patterns.dump', compact('name', 'description', 'item'));
    }

    /**
     * @link http://bigwriter.local/creational/factory-method
     */
    public function FactoryMethod()
    {
        $name = 'Фабричный метод (Factory Method)';
        $description = FactoryMethod::getDescription();

        //$dialogForm = new BootstrapDialogForm();
        $dialogForm = new SemanticUiDialogForm();
        $item = $dialogForm->render();

        Debugbar::info($item);

        return view('patterns.dump', compact('name', 'description', 'item'));
    }

    /**
     * @link http://bigwriter.local/creational/static-factory
     */
    public function StaticFactory()
    {
        $name = 'Статическая фабрика (Static factory)';
        $description = StaticFactory::getDescription();

        $appMailMessenger = StaticFactory::build('email');
        $appPhoneMessenger = StaticFactory::build('sms');

        $item = 1;

        Debugbar::info($appMailMessenger, $appPhoneMessenger);

        return view('patterns.dump', compact('name', 'description', 'item'));
    }

    /**
     * @link http://bigwriter.local/creational/simple-factory
     */
    public function SimpleFactory()
    {
        $name = 'Простая фабрика (Simple factory)';
        $description = MessengerSimpleFactory::getDescription();
        $factory = new MessengerSimpleFactory();

        $appMailMessenger = $factory->build('email');
        $appPhoneMessenger = $factory->build('sms');

        $item = 1;

        Debugbar::info($appMailMessenger, $appPhoneMessenger);

        return view('patterns.dump', compact('name', 'description', 'item'));
    }

    /**
     * Антипаттерн
     * @link http://bigwriter.local/creational/singleton
     */
    public function Singleton()
    {
        $name = 'Одиночка (Singleton)';
        $description = SimpleSingleton::getDescription();

        // Простой способ создания Одиночки (порождаем один и тот же объект)
        $result['simpleSingleton1'] = SimpleSingleton::getInstance();
        $result['simpleSingleton1']->setTest('simpleSingleton1');
        $result['simpleSingleton2'] = SimpleSingleton::getInstance();
        $result[] = $result['simpleSingleton1'] === $result['simpleSingleton2'];

        // Продвинутый способ создания Одиночки
        $result['advancedSingleton1'] = AdvancedSingleton::getInstance();
        $result['advancedSingleton1']->setTest('advancedSingleton1');
        $result['advancedSingleton2'] = AdvancedSingleton::getInstance();
        $result[] = $result['advancedSingleton1'] === $result['advancedSingleton2'];

        // Laravel-way способ создания Одиночки
        // add to AppServiceProvider code
        /**
         * public $singletons = [
         *     AnotherConnection::class => LaravelSingleton::class,
         * ];
         */
        $result['laravelSingleton1'] = app(AnotherConnection::class); # один и тот же
        $result['laravelSingleton1']->setTest('laravelSingleton1');
        $result['laravelSingleton2'] = app(AnotherConnection::class); # один и тот же
        $result['laravelSingleton3'] = new LaravelSingleton(); # тут уже другой
        $result[] = $result['laravelSingleton1'] === $result['laravelSingleton2']; # true
        $result[] = $result['laravelSingleton2'] === $result['laravelSingleton3']; # false

        Debugbar::info($result);

        $item = 1;

        return view('patterns.dump', compact('name', 'description', 'item'));
    }

    /**
     * @link http://bigwriter.local/creational/multiton
     */
    public function Multiton()
    {
        $name = 'Пул одиночек (Multiton)';
        $description = SimpleMultiton::getDescription();

        $multiton[] = SimpleMultiton::getInstance('mysql')->setTest('mysql-test');
        $multiton[] = SimpleMultiton::getInstance('mongo');

        $multiton[] = SimpleMultiton::getInstance('mysql');
        $multiton[] = SimpleMultiton::getInstance('mongo')->setTest('mongo-test');

        $simpleMultitonNext = SimpleMultitonNext::getInstance('mysql');
        $simpleMultitonNext->test2 = 'init';
        $multiton[] = $simpleMultitonNext;

        $simpleMultitonNext = SimpleMultitonNext::getInstance('mysql');
        $simpleMultitonNext->test2 = 'init2';
        $multiton[] = $simpleMultitonNext;

        Debugbar::info($multiton);

        $item = 1;

        return view('patterns.dump', compact('name', 'description', 'item'));
    }

    /**
     * @link http://bigwriter.local/creational/builder
     */
    public function Builder()
    {
        $name = 'Строитель (Builder)';
        $description = PageManager::getDescription();

        $builder = new PageBuilder();
        $pages[] = $builder->setTitle('from Builder')->getPage();

        $manager = new PageManager(); # Director
        $manager->setBuilder($builder);

        $pages[] = $manager->createCleanPage();
        $pages[] = $manager->createNewPageIt();
        $pages[] = $manager->createNewPageCats();


        Debugbar::info($pages);

        $item = 1;

        return view('patterns.dump', compact('name', 'description', 'item'));
    }

    /**
     * Суурогатный пример lazy load, не Laravel-way
     * @link http://bigwriter.local/creational/lazy-initialization
     */
    public function LazyInitialization()
    {
        $name = 'Ленивая загрузка. Отложенная инициализация. Lazy initialization (load)';
        $description = LazyInitialization::getDescription();

        $lazyLoad = new LazyInitialization();

        $user[] = $lazyLoad->getUser()->name;
        $user[] = $lazyLoad->getUser()->email;
        $user[] = $lazyLoad->getUser()->created_at;

        Debugbar::info($user);

        $item = 1;

        return view('patterns.dump', compact('name', 'description', 'item'));
    }

    /**
     * @link http://bigwriter.local/creational/prototype
     */
    public function Prototype()
    {
        $name = 'Прототип, клон (prototype, clone)';
        $description = PrototypeDemo::getDescription();

        $prototype = new PrototypeDemo();
        $result = $prototype->run();

        Debugbar::info($result);

        $item = 1;

        return view('patterns.dump', compact('name', 'description', 'item'));
    }

    /**
     * @link http://bigwriter.local/creational/object-pool
     */
    public function ObjectPool()
    {
        $name = 'Объектный пул (Object pool)';
        $description = ObjectPoolDemo::getDescription();

        $prototype = new ObjectPoolDemo();
        $result = $prototype->run();

        Debugbar::info($result);

        $item = 1;

        return view('patterns.dump', compact('name', 'description', 'item'));
    }
}
