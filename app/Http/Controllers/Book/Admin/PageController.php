<?php

namespace App\Http\Controllers\Book\Admin;

use App\Http\Requests\BookUpdateRequest;
use App\Http\Requests\PageCreateRequest;
use App\Http\Requests\PageRestoreRequest;
use App\Http\Requests\PageUpdateRequest;
use App\Jobs\PageAfterCreateJob;
use App\Jobs\PageAfterDeleteJob;
use App\Models\Book;
use App\Models\Page;
use App\Repositories\BookRepository;
use App\Repositories\PageRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PageController extends BaseController
{
    /**
     * @var BookRepository
     */
    private $bookRepository;

    /**
     * @var PageRepository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $pageRepository;

    public function __construct()
    {
        parent::__construct();
        $this->bookRepository = app(BookRepository::class);
        $this->pageRepository = app(PageRepository::class);
    }

    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        // $paginator = Book::paginate(5);
        $paginator = $this->pageRepository->getAllWithPaginate(20);
        return view('admin.pages.index', compact('paginator'));
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        $item = new Page();
        $booksList = $this->bookRepository->getForComboBox();

        return view('admin.pages.edit', compact('item', 'booksList'));
    }

    /**
     * Store a newly created resource in storage.
     * @param PageUpdateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PageCreateRequest $request)
    {
        $data = $request->input();

        $item = (new Page())->create($data);

        if ($item) {
            # запуск job
            $job = new PageAfterCreateJob($item);
            $this->dispatch($job);

            return redirect()
                ->route('admin.pages.model.edit', $item->id)
                ->with(['success' => 'Успешно сохранено']);
        } else {
            return back()
                ->withErrors(['msg' => 'Сохранение не удалось'])
                ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd(__METHOD__);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @param BookRepository $bookRepository
     * @param PageRepository $pageRepository
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id, BookRepository $bookRepository, PageRepository $pageRepository)
    {
        //$item = Book::findOrFail($id);
        //$userList = Book::all();

        $item = $this->pageRepository->getEdit($id);
        if (empty($item)) {
            abort(404);
        }

        $booksList = $this->bookRepository->getForComboBox();

        return view('admin.pages.edit', compact('item', 'booksList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function update(PageUpdateRequest $request, $id)
    {
        $item = $this->pageRepository->getEdit($id);
        if (empty($item)) {
            return back()
                ->withErrors(['msg' => "Запись с ИД: {$id} не найдена"])
                ->withInput();
        }
        $data = $request->all(); # analog -> $request->input();

        /* replace to observer
        if (empty($data['slug'])) {
            $data['slug'] = Str::of($data['title'])->slug();
        }*/
        $result = $item->update($data);

        if ($result) {
            return redirect()
                ->route('admin.pages.model.edit', $item->id)
                ->with(['success' => 'Успешно сохранено']);
        } else {
            return back()
                ->withErrors(['msg' => 'Сохранение не удалось'])
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        //dd(__METHOD__);

        $result = Page::destroy($id); # soft-deleting without removing from DB
        // result = Page::find($id)->forceDelete(); # deleting with removing from DB

        if ($result) {

            # варианты Job
            PageAfterDeleteJob::dispatch($id)->delay(20); # delay 20 sec

            // PageAfterDeleteJob::dispatchNow($id);

            /*dispatch(new PageAfterDeleteJob($id)); # through helper
            dispatch(new PageAfterDeleteJob($id))->delay(20); # through helper - delay 20 sec
            dispatch_now(new PageAfterDeleteJob($id)); # through helper

            $this->dispatch(new PageAfterDeleteJob($id));
            $this->dispatchNow(new PageAfterDeleteJob($id));*/

            return redirect()
                ->route('admin.pages.model.index')
                ->with(['success' => 'Запись'.$id.'перемещена в корзину! <a href="'.route('admin.pages.model.restore', $id).'">Восстановить</a>']);
        } else {
            return back()
                ->withErrors(['msg' => 'Удаление не выполнилось'])
                ->withInput();
        }
    }

    /**
     * @param PageRestoreRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore(PageRestoreRequest $request, $id)
    {
        $result = Page::withTrashed()->find($id)->restore();

        if ($result) {
            return redirect()
                ->route('admin.pages.model.edit', $id)
                ->with(['success' => 'Запись ' . $id . ' восстановлена!']);
        }

        return back()->withErrors(['msg' => 'Ошибка восстановления!']);
    }
}
