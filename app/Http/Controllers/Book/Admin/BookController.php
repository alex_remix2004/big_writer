<?php

namespace App\Http\Controllers\Book\Admin;

use App\Http\Requests\BookUpdateRequest;
use App\Models\Book;
use App\Repositories\BookRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BookController extends BaseController
{
    /**
     * @var BookRepository
     */
    private $bookRepository;

    /**
     * @var UserRepository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $userRepository;

    public function __construct()
    {
        parent::__construct();
        $this->bookRepository = app(BookRepository::class);
        $this->userRepository = app(UserRepository::class);
    }

    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        // $paginator = Book::paginate(5);
        $paginator = $this->bookRepository->getAllWithPaginate(5);
        return view('admin.books.index', compact('paginator'));
    }

    /**
     * Show the form for creating a new resource.
     * @param UserRepository $userRepository
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(UserRepository $userRepository)
    {
        $item = new Book();
        $usersList = $this->userRepository->getForComboBox();

        return view('admin.books.edit', compact('item', 'usersList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(BookUpdateRequest $request)
    {
        $data = $request->input();
        if (empty($data['slug'])) {
            $data['slug'] = Str::of($data['title'])->slug();
        }

        $item = (new Book())->create($data);

        if ($item) {
            return redirect()
                ->route('admin.books.model.edit', $item->id)
                ->with(['success' => 'Успешно сохранено']);
        } else {
            return back()
                ->withErrors(['msg' => 'Сохранение не удалось'])
                ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd(__METHOD__);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @param BookRepository $bookRepository
     * @param UserRepository $userRepository
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id, BookRepository $bookRepository, UserRepository $userRepository)
    {
        /*$item = Book::findOrFail($id);
        $userList = Book::all();*/

        $item = $this->bookRepository->getEdit($id);
        if (empty($item)) {
            abort(404);
        }
        $usersList = $this->userRepository->getForComboBox();

        return view('admin.books.edit', compact('item', 'usersList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function update(BookUpdateRequest $request, $id)
    {
        // $item = Book::find($id);
        $item = $this->bookRepository->getEdit($id);
        if (empty($item)) {
            return back()
                ->withErrors(['msg' => "Запись с ИД: {$id} не найдена"])
                ->withInput();
        }

        $data = $request->all(); # analog -> $request->input();
        /* replace to observer
         * if (empty($data['slug'])) {
            $data['slug'] = Str::of($data['title'])->slug();
        }*/
        $result = $item->fill($data)->save(); # or $result = $item->update($data);

        if ($result) {
            return redirect()
                ->route('admin.books.model.edit', $item->id)
                ->with(['success' => 'Успешно сохранено']);
        } else {
            return back()
                ->withErrors(['msg' => 'Сохранение не удалось'])
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        dd(__METHOD__);
    }
}
