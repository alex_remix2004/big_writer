<?php

namespace App\Http\Controllers;

use App\Patterns\Structural\Adapter\Classes\MediaLibraryAdapter;
use App\Patterns\Structural\Adapter\Classes\MediaLibrarySelfWritten;
use App\Patterns\Structural\Adapter\Interfaces\MediaLibraryInterface;
use App\Patterns\Structural\Facade\Entities\Order;
use App\Patterns\Structural\Facade\OrderSaveFacade;
use Debugbar;
use Illuminate\Http\Request;

class StructuralPatternsController extends Controller
{
    /**
     * @link http://bigwriter.local/structural/adapter
     */
    public function Adapter()
    {
        $name = 'Адаптер (Adapter / Wrapper)';
        $description = MediaLibraryAdapter::getDescription();

        /**
         * подключение вручную через конкретный класс
         * @var MediaLibraryInterface $mediaLibrary
         */
        //$mediaLibrary = app(MediaLibraryAdapter::class);
        //$mediaLibrary = app(MediaLibrarySelfWritten::class);

        /**
         *  подлючение через интерфейс, который подключен ко всем классам как адаптер,
         *  чтобы узнать какой класс отдавать интерфейсу надо в AppServiceProver добавить:
         *
         *  public $bindings = [
         *     MediaLibraryInterface::class => MediaLibraryAdapter::class,
         *  ];
         */
        $mediaLibrary = app(MediaLibraryInterface::class);

        $result[] = $mediaLibrary->upload('ImageFile');
        $result[] = $mediaLibrary->get('ImageFile');

        $item = 1;

        Debugbar::info($result);

        return view('patterns.dump', compact('name', 'description', 'item'));
    }

    /**
     * @link http://bigwriter.local/structural/facade
     */
    public function Facade()
    {
        $name = 'Фасад (Facade)';
        $description = OrderSaveFacade::getDescription();

        $order = new Order();
        $data = \request()->all(); # лучше использовать паттерн DDO

        (new OrderSaveFacade())->save($order, $data); # более-менее верно

        $item = 1;

        //Debugbar::info($result);

        return view('patterns.dump', compact('name', 'description', 'item'));
    }
}
