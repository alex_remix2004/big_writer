<?php

namespace App\Http\Controllers;

use App\Jobs\GenerateCatalog\GenerateCatalogMainJob;
use App\Models\Page;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DiggingDeeperController extends Controller
{
    public  function collections()
    {
        /**
         * @var \Illuminate\Database\Eloquent\Collection $eloquentCollection
         */
        $eloquentCollection = Page::withTrashed()->get();

        //dd(__METHOD__, $eloquentCollection, $eloquentCollection->toArray());

        /**
         * @var \Illuminate\Support\Collection $collection
         */
        $collection = collect($eloquentCollection->toArray());

        /*dd(
            get_class($eloquentCollection),
            get_class($collection),
            $collection
        );*/

        /*$result['first'] = $collection->first();
        $result['last'] = $collection->last();*/

        /*$result['where']['data'] = $collection
            ->where('book_id', 1)
            ->values()
            ->keyBy('id');*/

        /*$result['where']['count'] = $result['where']['data']->count();
        $result['where']['isEmpty'] = $result['where']['data']->isEmpty();
        $result['where']['isNotEmpty'] = $result['where']['data']->isNotEmpty();*/


        //$result['where']['first_where'] = $result['where']['data']->firstWhere('created_at', '>', '2020-10-23 03:19:10');

        // map() исходную коллекцию оставляет, создает новую
        /*$result['map']['all'] = $collection->map(function (array $item) {
            $newItem = new \stdClass();
            $newItem->item_id = $item['id'];
            $newItem->item_name = $item['title'];
            $newItem->exists = is_null($item['deleted_at']);

            return $newItem;
        });

        $result['map']['soft_deleted'] = $result['map']['all']->where('exists', '=', false)->values();*/

        // transform() исходную коллекцию изменяет не создавая новую
        $collection->transform(function (array $item) {
            $newItem = new \stdClass();
            $newItem->item_id = $item['id'];
            $newItem->item_name = $item['title'];
            $newItem->exists = is_null($item['deleted_at']);
            $newItem->created_at = Carbon::parse($item['created_at']);

            return $newItem;
        });


        /*$newItem = new \stdClass();
        $newItem->id = 888;

        $newItem2 = new \stdClass();
        $newItem2->id = 999;


        $prepend = $collection->prepend($newItem)->first();
        $push = $collection->push($newItem2)->last();
        $pull = $collection->pull(1);

        dd(compact('collection', 'prepend', 'push', 'pull'));*/

        // Фильтрация - замена orWhere()
        /*$filtered = $collection->filter(function ($item) {
            $byDay = $item->created_at->isFriday();
            $byDate = $item->created_at->day == 11;

            return $byDay && $byDate;
        });

        dd(compact('filtered'));*/

        $sortedSimpleCollection = collect([4, 5, 2, 1, 3])->sort()->values();
        $sortedAscCollection = $collection->sortBy('created_at');
        $sortedDescCollection = $collection->sortByDesc('item_id');

        dd(compact('sortedSimpleCollection', 'sortedAscCollection', 'sortedDescCollection'));
    }

    /**
     * @link http://bigwriter.local/digging_deeper/prepare-catalog
     *
     * php artisan queue:listen --queue=generate-catalog --tries=3 --delay=10
     */
    public function prepareCatalog()
    {
        GenerateCatalogMainJob::dispatch();
    }
}
