<?php

namespace App\Http\Controllers;

use App\Patterns\Fundametals\Delegation\AppMessenger;
use App\Patterns\Fundametals\EventChannel\EventChannelJob;
use App\Patterns\Fundametals\PropertyContainer\Page;
use App\Patterns\Fundametals\PropertyContainer\PropertyContainer;

class FundamentalPatternsControllerController extends Controller
{
    /**
     * @link http://bigwriter.local/fundamentals/property-container
     * @throws \Exception
     */
    public function PropertyContainer()
    {
        $name = 'Контейнер свойств (Property Container)';
        $description = PropertyContainer::getDescription();

        $item = new Page();

        $item->setTitle('Название');
        $item->setBook(10);

        $item->addProperty('view_count', 100);

        $item->addProperty('last_update', '2020-09-01');
        $item->setProperty('last_update', '2020-09-02');

        $item->addProperty('read_only', true);

        return view('patterns.dump', compact('name', 'description', 'item'));
    }

    /**
     * @link http://bigwriter.local/fundamentals/delegation
     * @throws \Exception
     */
    public function Delegation()
    {
        $name = 'Делегирование (Delegation)';
        $description = AppMessenger::getDescription();

        $item = new AppMessenger();

        $item->setSender('sender@email.net')
            ->setRecipient('recipient@email.net')
            ->setMessage('Hello, friends!')
            ->send();

        $item->toSms()
            ->setSender('somsender@email.net')
            ->setRecipient('somerecipient@email.net')
            ->setMessage('Again me, friends!')
            ->send();

        return view('patterns.dump', compact('name', 'description', 'item'));
    }

    /**
     * Похожие на него Observer и Издатель-подписчик
     * @link http://bigwriter.local/fundamentals/event-channel
     */
    public function EventChannel()
    {
        $name = 'Канал событий (Event channel)';
        $description = EventChannelJob::getDescription();

        $item = new EventChannelJob();
        $item->run();

        return view('patterns.dump', compact('name', 'description', 'item'));
    }

    /**
     * это не php-шный интерфейс
     * @link http://bigwriter.local/fundamentals/interface-pattern
     */
    public function InterfacePattern()
    {
        $name = 'Интерфейс (interface)';
        $description = EventChannelJob::getDescription();

        $item = new EventChannelJob();
        $item->run(); # метод - аналог интерфейса для вызова всех внутренних классов, которые оборачиваются в интерфейс

        return view('patterns.dump', compact('name', 'description', 'item'));
    }
}
