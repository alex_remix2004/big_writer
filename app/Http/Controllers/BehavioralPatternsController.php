<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Patterns\Behavioral\Strategy\SalaryManager;
use Carbon\Carbon;
use Debugbar;
use Illuminate\Http\Request;

class BehavioralPatternsController extends Controller
{
    /**
     * @link http://bigwriter.local/behavioral/strategy
     */
    public function Strategy()
    {
        $name = 'Стратегия (strategy)';
        $description = SalaryManager::getDescription();

        $period = [
            Carbon::now()->subMonth()->startOfMonth(),
            Carbon::now()->subMonth()->endOfMonth(),
        ];

        $users = User::all();

        $result = (new SalaryManager($period, $users))->handle();

        Debugbar::info($result);
        $item = 1;

        return view('patterns.dump', compact('name', 'description', 'item'));
    }
}
