<?php

namespace App\Repositories;

//use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Models\Page as Model;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class PageRepository.
 */
class PageRepository extends CoreRepository
{
    /**
     * @param $id
     * @return Model
     */
    public function getEdit($id)
    {
        return $this->startConditions()->find($id);
    }

    /**
     * @return string
     */
    protected function getModelClass()
    {
        return Model::class;
    }

    /**
     * @param null $perPage
     * @return LengthAwarePaginator
     */
    public function getAllWithPaginate($perPage = null)
    {
        $columns = ['id', 'title', 'book_id', 'slug', 'content_raw', 'content_html', 'created_at', 'updated_at',];

        $result = $this
            ->startConditions()
            ->select($columns)
            ->orderBy('id', 'DESC')
            ->with(['pageBook:id,title'])
            ->paginate($perPage);

        return $result;
    }
}
