<?php

namespace App\Repositories;

use App\Models\Book as Model;

/**
 * Class BookRepository.
 */
class BookRepository extends CoreRepository
{
    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Model::class;
    }

    /**
     * @param $id
     * @return Model
     */
    public function getEdit($id)
    {
        return $this->startConditions()->find($id);
    }

    /**
     * @param null $perPage
     * @return mixed
     */
    public function getAllWithPaginate($perPage = null)
    {
        $fields = ['id', 'title', 'user_id', 'created_at', 'slug'];

        $result = $this
            ->startConditions()
            ->select($fields)
            ->with(['user'])
            ->paginate($perPage);

        return $result;
    }

    public function getForComboBox()
    {
        $fields = implode(', ', ['id', 'CONCAT (id, ". ", title) AS id_title']);

        $result = $this
            ->startConditions()
            ->selectRaw($fields)
            ->toBase()
            ->get();

        return $result;
    }
}
