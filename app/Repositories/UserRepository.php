<?php

namespace App\Repositories;

use App\Models\User as Model;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class UserRepository.
 */
class UserRepository extends CoreRepository
{
    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Model::class;
    }

    /**
     * @param $id
     * @return Model
     */
    public function getEdit($id)
    {
        return $this->startConditions()->find($id);
    }


    public function getForComboBox()
    {
        // return $this->startConditions()->all();

        $fields = implode(', ', [
           'id',
           'CONCAT (id, ". ", name) AS id_name'
        ]);

        /*$result[] = $this->startConditions()->all();
        $result[] = $this
            ->startConditions()
            ->select(['users.*', \DB::raw('CONCAT (id, ". ", name) AS id_name')])
            ->toBase()
            ->get();*/

        $result = $this
            ->startConditions()
            ->selectRaw($fields)
            ->toBase()
            ->get();

        return $result;
    }

    private function getIdTitleAttribute() {
        return $this->id.'_'.$this->name;
    }
}
