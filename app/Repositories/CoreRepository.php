<?php

namespace App\Repositories;

/*use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;*/
use Illuminate\Database\Eloquent\Model;

/**
 * Class CoreRepository.
 */
abstract class CoreRepository /*extends BaseRepository*/
{
    /**
     * The repository model.
     *
     * @var Model
     */
    protected $model;

    public function __construct()
    {
        $this->model = app($this->getModelClass()); # or $this->model = new $this->getModelClass();
    }

    /**
     * @return mixed
     */
    abstract protected function getModelClass();

    /**
     * @return Model|\Illuminate\Foundation\Application|mixed
     */
    protected function startConditions()
    {
        return clone $this->model;
    }
}
