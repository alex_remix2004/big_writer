<?php

namespace App\Observers;

use App\Models\Book;
use Illuminate\Support\Str;

class BookObserver
{
    /**
     * Handle the Book "created" event.
     *
     * @param  \App\Models\Book  $book
     * @return void
     */
    public function created(Book $book)
    {
        $this->setUser($book);
    }

    /**
     * Handle the Book "updating" event before update the record.
     * This method was added by handle.
     *
     * @param  \App\Models\Book  $book
     * @return void
     */
    public function updating(Book $book)
    {
        /*$test[] = $book->isDirty(); # если что-то изменилось то..
        $test[] = $book->getDirty(); # в каких полях что изменилось..
        $test[] = $book->isDirty('created_at'); # если изменилось в данном атрибуте что-то, то ..
        $test[] = $book->isDirty('user_id');
        $test[] = $book->getAttribute('user_id'); # получение текущего измененного атрибута
        $test[] = $book->user_id; # получение текущего измененного атрибута
        $test[] = $book->getOriginal('user_id'); # получение оригинала неизмененного атрибута
        dd($test);*/

        $this->setSlug($book);
    }

    /**
     * Handle the Book "updated" event after update the record.
     *
     * @param  \App\Models\Book  $book
     * @return void
     */
    public function updated(Book $book)
    {
        //
    }

    /**
     * Handle the Book "deleted" event.
     *
     * @param  \App\Models\Book  $book
     * @return void
     */
    public function deleted(Book $book)
    {
        //
    }

    /**
     * Handle the Book "restored" event.
     *
     * @param  \App\Models\Book  $book
     * @return void
     */
    public function restored(Book $book)
    {
        //
    }

    /**
     * Handle the Book "force deleted" event.
     *
     * @param  \App\Models\Book  $book
     * @return void
     */
    public function forceDeleted(Book $book)
    {
        //
    }

    /**
     * @param Book $book
     */
    protected function setSlug(Book $book)
    {
        if (empty($book->slug)) {
            $book->slug = Str::of($book->title)->slug();
        }
    }

    /**
     * @param Book $book
     */
    protected function setUser(Book $book)
    {
        $book->user_id = auth()->id() ?? Book::UNKNOWN_USER;
    }
}
