<?php

namespace App\Observers;

use App\Models\Page;
use Illuminate\Support\Str;

class PageObserver
{
    /**
     * Handle the Page "creating" before create event.
     *
     * @param  \App\Models\Page  $page
     * @return void
     */
    public function creating(Page $page)
    {
        $this->setSlug($page);
        $this->setHtml($page);
    }

    /**
     * Handle the Page "created" event.
     *
     * @param  \App\Models\Page  $page
     * @return void
     */
    public function created(Page $page)
    {
        //
    }

    /**
     * Handle the Book "updating" event before update the record.
     * This method was added by handle.
     *
     * @param  \App\Models\Page  $page
     * @return void
     */
    public function updating(Page $page)
    {
        $this->setSlug($page);
    }

    /**
     * Handle the Page "updated" event.
     *
     * @param  \App\Models\Page  $page
     * @return void
     */
    public function updated(Page $page)
    {
        //
    }

    /**
     * Handle the Page "deleting" event and it was added by handle.
     *
     * @param  \App\Models\Page  $page
     * @return bool
     */
    public function deleting(Page $page)
    {
        // dd(__METHOD__, $page);
        //return false;
    }

    /**
     * Handle the Page "deleted" event.
     *
     * @param  \App\Models\Page  $page
     * @return void
     */
    public function deleted(Page $page)
    {
        //dd(__METHOD__, $page);
    }

    /**
     * Handle the Page "restored" event.
     *
     * @param  \App\Models\Page  $page
     * @return void
     */
    public function restored(Page $page)
    {
        //
    }

    /**
     * Handle the Page "force deleted" event.
     *
     * @param  \App\Models\Page  $page
     * @return void
     */
    public function forceDeleted(Page $page)
    {
        //
    }

    /**
     * @param Page $page
     */
    protected function setSlug(Page $page)
    {
        if (empty($page->slug)) {
            $page->slug = Str::of($page->title)->slug();
        }
    }

    protected function setHtml(Page $page)
    {
        if ($page->isDirty('content_raw')) {
            // TODO markdown -> html_text
            $page->content_html = $page->content_raw;
        }
    }
}
