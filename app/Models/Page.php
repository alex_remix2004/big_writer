<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Book;

/**
 * Class Page
 * @package App\Models
 *
 * @property-read Book $book
 * @property-read Book $pageBook
 * @property-read Book $pageBookTitle
 * @property string $title
 * @property string $slug
 * @property string $content_raw
 * @property string $content_html
 * @property integer $book_id
 * @property integer $id
 */
class Page extends Model
{
    use HasFactory, SoftDeletes;

    const FIRST_BOOK = 1;

    protected $fillable = ['title', 'slug', 'book_id', 'content_raw', 'description',];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function book()
    {
        return $this->belongsTo(Book::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pageBook()
    {
        //return $this->belongsTo(Book::class, 'book_id', 'id');
        return $this->belongsTo(Book::class, 'book_id', 'id')->withDefault(['title' => '???']);
    }

    /**
     * Accessor
     * @return string
     */
    public function getPageBookTitleAttribute(): string
    {
        return $this->pageBook->title ?? ($this->isFirstBook() ?? 'Первая книга(подмена)');
    }

    /**
     * @return bool
     */
    private function isFirstBook(): bool
    {
        return $this->book_id === Page::FIRST_BOOK;
    }

    /**
     * Accessor example
     * @param $valueFromObject
     * @return false|string|string[]
     *
     * public function getTitleAttribute($valueFromObject)
     * {
     *     return mb_strtoupper($valueFromObject);
     * }
     */

    /**
     * Mutator example
     * @param $incomingValue
     * @return false|string|string[]
     * public function setTitleAttribute($incomingValue)
     * {
     *     $this->attributes['title'] = mb_strtolower($incomingValue);
     * }
     */

}
