<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User;

/**
 * Class Book
 * @package App\Models
 *
 * @property User $user
 * @property User $authorBook
 * @property string $title
 * @property string $slug
 * @property string $description
 * @property integer $user_id
 */
class Book extends Model
{
    use HasFactory, SoftDeletes;

    const UNKNOWN_USER = 1;

    protected $fillable = ['title', 'slug', 'user_id', 'description',];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function authorBook()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
