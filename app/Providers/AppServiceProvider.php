<?php

namespace App\Providers;

use App\Models\Book;
use App\Models\Page;
use App\Observers\BookObserver;
use App\Observers\PageObserver;
use App\Patterns\Creational\Singleton\Contracts\AnotherConnection;
use App\Patterns\Creational\Singleton\LaravelSingleton;
use App\Patterns\Structural\Adapter\Classes\MediaLibraryAdapter;
use App\Patterns\Structural\Adapter\Interfaces\MediaLibraryInterface;
use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\Paginator;

class AppServiceProvider extends ServiceProvider
{
    public $singletons = [
        AnotherConnection::class => LaravelSingleton::class,
    ];

    public $bindings = [
        MediaLibraryInterface::class => MediaLibraryAdapter::class, # если добавить новую библиотеку, то это закоментировать и ниже новую подключить
        // MediaLibraryInterface::class => MediaLibraryAdapter2::class,
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();
        Book::observe(BookObserver::class);
        Page::observe(PageObserver::class);
    }
}
